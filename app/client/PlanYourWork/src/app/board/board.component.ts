import { switchMap, tap } from 'rxjs/operators';
import { BoardModel } from './../models/board.model';
import { BoardService } from './../services/board.service';
import { ListModel } from './../models/list.model';
import { Card } from './../models/card.model';
import { ListService } from './../services/list.service';
import { CardService } from './../services/card.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnDestroy {

  boardName: string;
  projectId: string;
  boardId: string;
  board: BoardModel;

  public lists: ListModel[] = [];
  private activeSubscriptions: Subscription[] = [];

  constructor(private boardService: BoardService,
              private listService: ListService,
              private cardService: CardService,
              private activatedRoute: ActivatedRoute,
              private toastrService: ToastrService) {

    const params = this.activatedRoute.paramMap.pipe(switchMap(params => {
      this.boardId = params.get('boardId');
      this.projectId = params.get('projectId');

      return this.boardService.getBoard(this.boardId).pipe(tap((board: BoardModel) => {
        this.board = board;
        this.boardName = board.name;
        this.lists = [];

        board.lists.forEach(list => {
          this.lists.push({"_id": list._id, "name": list.name, "tasks": list.tasks});
        });

        this.checkDuedate();

      }));
    })).subscribe();

    this.activeSubscriptions.push(params);
  }

  ngOnInit(): void {}

  ngOnDestroy(): void { this.activeSubscriptions.forEach(sub => sub.unsubscribe());}

  checkDuedate(): void{
    let card: Card;
    let today: Date = new Date();

    this.lists.forEach(list => {
      list.tasks.forEach(task => {
        const getCardSub = this.cardService.getCardById(task)
                               .subscribe((c) => {
                                  card = c;
                                  if(card.dueDate != null){
                                      let current: Date = new Date(card.dueDate);
                                      current.setHours(0, 0, 0)

                                      if(current < today){
                                        card.completionStatus = 'overdue';

                                        const updateDateSub = this.cardService
                                                                  .changeCardDueDate(card._id, card.dueDate, card.completionStatus)
                                                                  .subscribe();
                                        this.activeSubscriptions.push(updateDateSub);
                                    }
                                  }
                               });
        this.activeSubscriptions.push(getCardSub);


      });
    });
  }

  onCreateList(): void {
    let listName: string = (document.getElementById("listName") as HTMLInputElement).value.trim();
    (document.getElementById("listName") as HTMLInputElement).value = "";

    if (listName === "") {
      this.toastrService.error("You can't add a list without a name!", 'Error');
      return;
    }

    const sub = this.listService.addNewList(listName).pipe(switchMap((list: ListModel) => {
      this.lists.push(list);
      return this.boardService.updateBoard(this.boardId, null, list._id);

    })).subscribe();

    this.activeSubscriptions.push(sub);
  }

  deleteList(listId: string): void {
    let listIndex: number = this.lists.findIndex((list) => list._id === listId);

    if (listIndex === -1) {
      return;
    }

    this.lists.splice(listIndex, 1);
    const sub = this.listService.deleteList(this.boardId,listId).subscribe();

    this.activeSubscriptions.push(sub);
  }

  changeBoardName(event: Event): void {
    const boardName = (event.target as HTMLInputElement).value.trim();

    if (this.boardName === boardName)
    return;

    if (boardName === "")
    {
      (event.target as HTMLInputElement).value = this.boardName;
      this.toastrService.error("You must provide a name for the board!", 'Error');
      return;
    }

    this.boardName = boardName;

    const sub = this.boardService.updateBoard(this.boardId, this.boardName, null).subscribe(() => {
    const  subRefresh =   this.boardService.refreshBoards(this.projectId).subscribe();
    this.activeSubscriptions.push(subRefresh);
    });

    this.activeSubscriptions.push(sub);
  }
}
