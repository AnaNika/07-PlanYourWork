import { Checklist } from './checklist.model';

export interface Card {
  _id: string;
  listId: string;
  title: string;
  description: string;
  members: string[];
  labels: string[];
  checklists: string[] | Checklist[];
  dueDate: Date;
  completionStatus: string;
  fileUrls: string[];
  hideAttachments: boolean;
  hideAttachmentsText: string;
  cover: string;
}
